<?php

class Ajax_Filters_Trainee {

    public function __construct() {

        $this->load_dependencies();

        add_action( 'wp_print_styles', array( $this, 'enqueue_styles' ) );
        add_action( 'admin_print_styles', array( $this, 'enqueue_styles' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'widgets_init', array( $this, 'register_widget_ajax_filters' ) );

    }

    private function load_dependencies() {

        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-widget-ajax-filters.php';

    }

    public function enqueue_styles() {
        wp_enqueue_style('jquery-ui-datepicker-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('ajax_filters_widget_plugin', plugins_url('ajax-filters-trainee') . '/css/style.css', false, false, 'all');
    }

    public function enqueue_scripts() {
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('ajax_filters_widget_ajax', plugins_url('ajax-filters-trainee') . '/js/ajax.js', false, false, true);
        wp_enqueue_script('ajax_filters_widget_datepicker', plugins_url('ajax-filters-trainee') . '/js/init_datepicker.js', array('jquery'), false, true);
        wp_localize_script( 'ajax_filters_widget_ajax', 'widget_ajax_filters_ajaxurl',
            array(
                'url' => admin_url('admin-ajax.php')
            )
        );
    }

    public function register_widget_ajax_filters() {
        register_widget( 'Widget_Ajax_Filters' );
    }

}
