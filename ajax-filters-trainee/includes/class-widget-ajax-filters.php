<?php

class Widget_Ajax_Filters extends WP_Widget {

    public function __construct(){
        parent::__construct(
            'ajax_filters_widget',
            __('Ajax Filters', 'ajax_filters_widget_domain'),
            array( 'description' => 'Просмотр ссылок постов.' )
        );
        wp_localize_script( 'ajax_filters_widget_ajax', 'widget_ajax_filters_ajaxurl',
            array(
                'url' => admin_url('admin-ajax.php')
            )
        );

        add_action( 'wp_ajax_widget_ajax_filters', array( $this, 'ajax' ) );
        add_action( 'wp_ajax_nopriv_widget_ajax_filters', array( $this, 'ajax' ) );
        add_filter( 'posts_search', array( $this, 'wph_search_by_title' ), 500, 2 );
    }

    public function widget( $args, $instance ) {
        ?>
            <div class="widget-ajax-filters-trainee">
                <div class="widget-ajax-filters-trainee-title">- Ссылки (ajax) -</div>
                <div class="widget-ajax-filters-trainee-ajaxform">
                    <input type="text" name="widget-ajax-filters-trainee-ajaxform-title" value="" placeholder="<?php _e('Заголовок'); ?>" />
                    <input type="text" id="widget-ajax-filters-trainee-ajaxform-datepicker" name="widget-ajax-filters-trainee-ajaxform-date" value="" placeholder="<?php _e('Выберите дату'); ?>" />
                    <?php _e("Количество ссылок"); ?>:<br />
                    <input type="number" name="widget-ajax-filters-trainee-ajaxform-numlinks" value="<?php echo $instance['number_of_links']; ?>" />
                </div>
                <div id="widget-ajax-filters-trainee-content">
                    &nbsp;
                </div>
            </div>
        <?php
    }

    public function form( $instance ) {
        $number_of_links = "5";

        if ( !empty( $instance ) ) {
            $number_of_links = $instance[ "number_of_links" ];
        }
        $number_of_links_id = $this->get_field_id( "number_of_links" );
        $number_of_links_name = $this->get_field_name( "number_of_links" );
        ?>
            <label for="<?php echo $number_of_links_id; ?>"><?php _e( 'Показывать количество ссылок', 'links-trainee' ); ?>:</label><br />
            <input id="<?php echo $number_of_links_id; ?>" type="number" name="<?php echo $number_of_links_name; ?>" value="<?php echo $number_of_links; ?>" /><br />
        <?php
    }

    public function update( $new_instance, $old_instance )  {
        $values = array();
        $values[ "number_of_links" ] = htmlentities( $new_instance[ "number_of_links" ] );
        return $values;
    }

    public function ajax() {
        $numlinks = intval( $_POST[ 'numlinks' ] );
        if( $numlinks == 0 ) {
            $numlinks = 5;
        }

        $title = trim( $_POST[ 'title' ] );
        $date = trim( $_POST[ 'date' ] );

        $args = array(
            'posts_per_page' => $numlinks,
            'orderby'        => 'date',
            'order'          => 'DESC',
            'post_type'      => 'post',
        );

        if( !empty( $title ) or !empty( $date ) ) {
            if( !empty( $title ) ) {
                $args[ 's' ] = $title;
            }
            if( !empty( $date ) ) {
                $date_explode = explode( '.', $date );
                $args[ 'date_query' ] = array(
                    'relation' => 'AND',
                    array(
                        'after' => array(
                            'year'  => $date_explode[2],
                            'month' => $date_explode[1],
                            'day'   => $date_explode[0],
                        ),
                        'inclusive' => true,
                    ),
                );
            }

        }
        $query = new WP_Query( $args );

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
                ?>
                &#8226; <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" target="_blank"><?php the_title(); ?></a><br />
                <?php
            }
        } else {
            ?>
                <div class="not_found"><?php _e( "Не найдено" ); ?></div>
            <?php
        }
        wp_reset_postdata();

        wp_die();
    }

    public function wph_search_by_title( $search, &$wp_query ) {
        global $wpdb;
        if (empty($search)) return $search;

        $q = $wp_query->query_vars;
        $n = !empty($q['exact']) ? '' : '%';
        $search = $searchand = '';

        foreach ((array) $q['search_terms'] as $term) {
            $term = esc_sql(like_escape($term));
            $search.="{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
            $searchand = ' AND ';
        }

        if (!empty($search)) {
            $search = " AND ({$search}) ";
            if (!is_user_logged_in())
                $search .= " AND ($wpdb->posts.post_password = '') ";
        }
        return $search;
    }

}
