jQuery(document).ready(function($) {
    widgetAjaxFiltersTraineeAjaxForm($);
});

jQuery("input[name='widget-ajax-filters-trainee-ajaxform-title']").on('keyup', function($){
    widgetAjaxFiltersTraineeAjaxForm($);
});
jQuery("input[name='widget-ajax-filters-trainee-ajaxform-date']").on('change', function($){
    widgetAjaxFiltersTraineeAjaxForm($);
});
jQuery("input[name='widget-ajax-filters-trainee-ajaxform-numlinks']").on('change', function($){
    widgetAjaxFiltersTraineeAjaxForm($);
});
jQuery("input[name='widget-ajax-filters-trainee-ajaxform-numlinks']").on('keyup', function($){
    widgetAjaxFiltersTraineeAjaxForm($);
});

function widgetAjaxFiltersTraineeAjaxForm($) {
    var data = {
        action: 'widget_ajax_filters',
        numlinks: jQuery("input[name='widget-ajax-filters-trainee-ajaxform-numlinks']").val(),
        title: jQuery("input[name='widget-ajax-filters-trainee-ajaxform-title']").val(),
        date: jQuery("input[name='widget-ajax-filters-trainee-ajaxform-date']").val()
    };

    jQuery.post(widget_ajax_filters_ajaxurl, data, function(response) {
        jQuery('#widget-ajax-filters-trainee-content').html(response);
    });
}
